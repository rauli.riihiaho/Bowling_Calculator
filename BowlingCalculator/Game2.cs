﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BowlingCalculator
{
    public class Game2
    {
        public List<int> _heitot = new List<int>();
        public List<int> _pisteetPerFrame = new List<int>();
        public int Pisteet { get; set; }
        public int FrameIndex;




        public void LisaaHeitto(int keila)
        {
            _heitot.Add(keila);
        }
        public int Roll()
        {



            FrameIndex = 0;
            int frameTauluun;
            for (int heittoLuku = 0; heittoLuku < 10; heittoLuku++)
            {
                if (onkoKaato(_heitot[FrameIndex]))
                {
                    Pisteet += kaatoBonus(FrameIndex);
                    frameTauluun = kaatoBonus(FrameIndex);
                    _pisteetPerFrame.Add(frameTauluun);
                    FrameIndex++;

                }
                else if (onkoOsittainen(_heitot[FrameIndex]))
                {
                    Pisteet += osittainBonus(FrameIndex);
                    frameTauluun = osittainBonus(FrameIndex);
                    _pisteetPerFrame.Add(frameTauluun);
                    FrameIndex += 2;
                }
                else
                {
                    Pisteet += pallotRuudussa(FrameIndex);
                    frameTauluun = pallotRuudussa(FrameIndex);
                    _pisteetPerFrame.Add(frameTauluun);
                    FrameIndex += 2;
                }



            }



            return Pisteet;
        }

        public Boolean onkoKaato(int heittojenMäärä)
        {
            return _heitot[heittojenMäärä] == 10;
        }

        public Boolean onkoOsittainen(int heittojenMäärä)
        {
            return _heitot[heittojenMäärä] + _heitot[heittojenMäärä + 1] == 10;
        }


        private int pallotRuudussa(int ruutuIndex)
        {
            return _heitot[ruutuIndex] + _heitot[ruutuIndex + 1];
        }

        private int osittainBonus(int ruutuIndex)
        {
            return _heitot[ruutuIndex + 2] + 10;
        }

        private int kaatoBonus(int ruutuIndex)
        {
            return _heitot[ruutuIndex + 1] + _heitot[ruutuIndex + 2] + 10;
        }

        public int haeYhdenFraminTulos(int ruutu)
        {
            return _pisteetPerFrame[ruutu];
        }

        public int palautaFramiMäärä()
        {
            return _pisteetPerFrame.Count();
        }

        public int palautaPisteet()
        {
            return Pisteet;
        }
    }
}
