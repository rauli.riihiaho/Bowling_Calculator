﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BowlingCalculator
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Anna keilauksen tulokset antamalla kaadettujen keilojen määrä erotettuna välilyönnillä (Esim. 10 9 2 3 jne)");
            string keilat = Console.ReadLine();
            int[] erillisetKeilat = keilat.Split(' ').Select(n => Convert.ToInt32(n)).ToArray();
            int keilojenMaara = erillisetKeilat.Count();
            int arvo;
            Game2 oneline = new Game2();

            for (int i = 0; i < keilojenMaara; i++)
            {
                arvo = erillisetKeilat[i];
                oneline.LisaaHeitto(arvo);
            }

            oneline.Roll();
            int freimit = oneline.palautaFramiMäärä();

            for (int o = 0; o < freimit; o++)
            {
                int ruutu = o + 1;
                Console.WriteLine("Ruudun " + ruutu + " tulos on " + oneline.haeYhdenFraminTulos(o));
            }

            Console.WriteLine("Koko pelin tulos on "+ oneline.palautaPisteet());
        }
    }
}
