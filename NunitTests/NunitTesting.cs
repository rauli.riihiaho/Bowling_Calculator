﻿using NUnit.Framework;
using BowlingCalculator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NunitTests
{
    [TestFixture]
    public class NunitTesting
    {
        
        [TestCase]
        public void LuoPeli()
        {
            var game = new Game2();
        }
        /*  
      [TestCase] Ei toimi enää koska logiikka on muuttunut.
      public void KaikkiOhi()
      {
          var game = new Game2();
          for (int i = 0; i < 20; i++)
          {
              game.Roll(0);

          }
          Assert.AreEqual(0, game.Pisteet);
      }

      [TestCase] Ei toimi enää koska logiikka on muuttunut.
      public void KaikkiYkkosia()
      {
          var game = new Game2();
          for (int i = 0; i < 20; i++)
          {
              game.Roll();

          }
          Assert.AreEqual(20, game.Pisteet);
      }
      */

        [TestCase]
        public void KaikkiYkkösia()
        {
            var game = new Game2();
            for (int i = 0; i < 20; i++)
            {
                game.LisaaHeitto(1);

            }
            game.Roll();
            Assert.AreEqual(20, game.Pisteet);

        }

        [TestCase]
        public void KaikkiKaatoja()
        {
            var game = new Game2();
            for (int i = 0; i < 12; i++)
            {
                game.LisaaHeitto(10);
            }
            game.Roll();
            Assert.AreEqual(300, game.Pisteet);
        }

        [TestCase]

        public void OsitusPeli()
        {
            var game = new Game2();
            for (int i = 0; i < 21; i++)
            {
                game.LisaaHeitto(5);
            }
            game.Roll();
            Assert.AreEqual(150, game.Pisteet);
        }
        
        [TestCase]

        public void TestaaYhdenFramenPalautus()
        {
            var game = new Game2();
            for (int i = 0; i < 21; i++)
            {
                game.LisaaHeitto(2);
            }
            game.Roll();
            Assert.AreEqual(4, game.haeYhdenFraminTulos(2));
        }

        [TestCase]

        public void TestaaFramiCount()
        {
            var game = new Game2();
            for (int i = 0; i < 21; i++)
            {
                game.LisaaHeitto(2);
            }
            game.Roll();
            Assert.AreEqual(10, game.palautaFramiMäärä());
        }

    }
}
